package com.crm.aks;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public class OrderPunchIn extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_punch_in);
    }

    public void addList(View view) {
        Dialog dialogg = new Dialog(OrderPunchIn.this, R.style.PauseDialog);
        dialogg.setContentView(R.layout.qr_model);

        RelativeLayout home = dialogg.findViewById(R.id.productList);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(OrderPunchIn.this, PunchIn_ProductList.class);
                startActivity(home);
                dialogg.dismiss();
                OrderPunchIn.this.finish();
            }
        });

        Window window = dialogg.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        dialogg.show();
    }
}