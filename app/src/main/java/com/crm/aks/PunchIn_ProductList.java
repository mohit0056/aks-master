package com.crm.aks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class PunchIn_ProductList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punch_in_product_list);
    }

    public void orderDetails(View view) {
        Intent intent=new Intent(PunchIn_ProductList.this, OrderReports.class);
        startActivity(intent);
        finish();
    }
}