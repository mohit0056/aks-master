package com.crm.aks;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

class OrderReportAdapter extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public OrderReportAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                OrderReportPunchIn OrderReportPunchInFragment = new OrderReportPunchIn();
                return OrderReportPunchInFragment;
            case 1:
                OrderReportPunchOut OrderReportPunchOutFragment = new OrderReportPunchOut();
                return OrderReportPunchOutFragment;
//            case 2:
//                wallh wallhFragment = new wallh();
//                return wallhFragment;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}