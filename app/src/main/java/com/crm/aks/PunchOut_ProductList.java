package com.crm.aks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class PunchOut_ProductList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punch_out_product_list);
    }

    public void orderReports(View view) {

        Intent intent=new Intent(PunchOut_ProductList.this, OrderReports.class);
        startActivity(intent);
        finish();
    }
}