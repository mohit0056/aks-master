package com.crm.aks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Order extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
    }

    public void punchIn(View view) {

        Intent intent=new Intent(Order.this, OrderPunchIn.class);
        startActivity(intent);
        finish();
    }
}