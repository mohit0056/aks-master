package com.crm.aks;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private DrawerLayout mDrawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.vNavigation);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

    }

    public void orders(View view) {

        Intent intent=new Intent(MainActivity.this, Order.class);
        startActivity(intent);
        finish();

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    public void openDrawar(View view) {
        mDrawer.openDrawer(Gravity.LEFT);

    }
}